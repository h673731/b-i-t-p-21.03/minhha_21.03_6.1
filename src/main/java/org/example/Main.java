package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Student> studentList = new ArrayList<>();

        int numberStudents;
        do {
            System.out.print("Nhập số lượng học sinh: ");
            numberStudents = scanner.nextInt();
        } while (numberStudents <= 0);

        for (int i = 0; i < numberStudents; i++) {
            System.out.println("Nhập thông tin cho học sinh thứ " + (i + 1));

            System.out.print("ID: ");
            int id = scanner.nextInt();
            scanner.nextLine();

            System.out.print("Ten: ");
            String name = scanner.nextLine();

            System.out.print("Tuoi: ");
            int age = scanner.nextInt();
            scanner.nextLine();

            System.out.print("Dia chi: ");
            String address = scanner.nextLine();

            System.out.print("Diem Toan: ");
            double math = scanner.nextDouble();

            System.out.print("Diem Ly: ");
            double physics = scanner.nextDouble();

            System.out.print("Diem Hoa: ");
            double chemistry = scanner.nextDouble();

            studentList.add(new Student(id, name, age, address, math, physics, chemistry));
        }
        for (int i = 0; i < numberStudents; i++) {
            System.out.println("Thông tin cho học sinh thứ " + (i + 1) + " la:");
            for (Student student : studentList) {
                student.display();
            }
        }

        //In diem trung binh
        for (Student student : studentList) {
            System.out.println("Diem trung binh cua " + student.getName() + " la: " + student.getAverage());
        }
        System.out.println("-----------------------------------------------------------------");

        //Sap xep danh sach hoc sinh theo diem trung binh
        for (int i = 0; i < studentList.size() - 1; i++) {
            for (int j = 0; j < studentList.size() - i - 1; j++) {
                double average1 = studentList.get(j).getAverage();
                double average2 = studentList.get(j + 1).getAverage();
                if (average1 > average2) {
                    Student temp = studentList.get(j);
                    studentList.set(j, studentList.get(j + 1));
                    studentList.set(j + 1, temp);
                }
            }
        }

        System.out.print("Danh sach hoc sinh sap xep theo diem trung binh: ");
        for (int i = 0; i < studentList.size(); i++) {
            System.out.print(studentList.get(i).getName());
            if (i < studentList.size() - 1) {
                System.out.print(", ");
            }
        }
        System.out.println();
        System.out.println("-----------------------------------------------------------------");

        //Diem toan cao nhat
        double highestMath = 0;
        Student studentWithHighestMath = null;

        for (Student student : studentList) {
            double math = student.getMath();
            if (math > highestMath) {
                highestMath = math;
                studentWithHighestMath = student;
            }
        }

        if (studentWithHighestMath != null) {
            System.out.println("Diem toan cao nhat la: " + highestMath + " cua hoc sinh " + studentWithHighestMath.getName());
        }
        System.out.println("-----------------------------------------------------------------");

        //Tuoi > 23
        System.out.print("Danh sach hoc sinh co tuoi > 23: ");
        boolean studentAbove23 = false;
        for (Student student : studentList) {
            if (student.getAge() > 23) {
                System.out.print(student.getName());
                System.out.print(", ");
                studentAbove23 = true;
            }
        }
        if (!studentAbove23) {
            System.out.print("Khong co hoc sinh nao!");
        }
        System.out.println();
        System.out.println("-----------------------------------------------------------------");

        //Ho Hoang
        boolean hoHoang = false;
        System.out.print("Danh sach hoc sinh co ho la Hoàng: ");
        for (Student student : studentList) {
            if (student.getName().startsWith("Hoàng")
                    || student.getName().startsWith("Hoang")) {
                System.out.print(student.getName());
                hoHoang = true;
            }
        }
        if (!hoHoang) {
            System.out.print("Khong co hoc sinh nao!");
        }
        System.out.println();
        System.out.println("-----------------------------------------------------------------");

        //Dia chi HN
        boolean diaChiHaNoi = false;
        System.out.print("Danh sach hoc sinh co dia chi o Ha Noi: ");
        for (Student student : studentList) {
            if (student.getAddress().contains("hn")) {
                System.out.print(student.getName() + ", ");
                diaChiHaNoi = true;
            }
        }
        if (!diaChiHaNoi) {
            System.out.print("Khong co hoc sinh nao!");
        }
    }
}