package org.example;

import java.util.Scanner;

public class Student extends Person {
    private int id;
    private double math;
    private double physics;
    private double chemistry;
    private double average;
    private static int nextId = 1;

    public Student() {
        this.id = nextId++;
    }

    public Student(int id, String name, int age, String address, double math, double physics, double chemistry) {
        super(id, name, age, address);
        this.math = math;
        this.physics = physics;
        this.chemistry = chemistry;
        this.id = nextId++;
    }

    public double getAverage() {
        return average = (math + physics + chemistry) / 3;
    }

    public double getMath() {
        return math;
    }

    @Override
    public void display() {
        super.display();
        System.out.println("Diem Toan: " + math);
        System.out.println("Diem Ly: " + physics);
        System.out.println("Diem Hoa: " + chemistry);
        System.out.println("Diem trung binh: " + average);
    }

    public static int getNextId() {
        return nextId;
    }

    public static void incrementNextId() {
        nextId++;
    }
}
